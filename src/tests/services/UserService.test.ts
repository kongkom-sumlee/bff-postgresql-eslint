import userRepository from '../../data/repositories/User.Repository'
import userService from '../../services/UserService'
import { CreateUserRequest } from '../../data/interfaces/requests/CreateUser.Request'
import userServiceHelper from '../../services/UserServiceHelper'
import { UpdateUserByIdRequest } from '../../data/interfaces/requests/UpdateUserById.Request'

jest.mock('sequelize')
jest.mock('../../data/repositories/User.Repository.ts')
jest.mock('../../services/UserServiceHelper.ts')

describe('UserService', () => {
  describe('getAllUser', () => {
    let getAllUserResponseSuccess: any
    let mockGetAllUser: jest.MockedFunction<typeof userRepository.getAllUser>

    beforeAll(() => {
      mockGetAllUser = userRepository.getAllUser as jest.MockedFunction<typeof userRepository.getAllUser>
    })

    beforeEach(() => {
      getAllUserResponseSuccess = {
        data: [
          {
            id: 1,
            name: 'example',
            email: 'example@email.com',
            createdAt: '2023-08-03T15:56:17.289Z',
            updatedAt: '2023-08-03T15:56:17.289Z'
          }
        ]
      }
    })

    afterEach(() => {
      jest.resetAllMocks()
    })

    test('when input is valid then return successfully', async () => {
      mockGetAllUser.mockReturnValueOnce(getAllUserResponseSuccess.data)
      const response = await userService.getAllUser()
      expect(response).toMatchObject(getAllUserResponseSuccess.data)
    })
  })

  describe('getUserById', () => {
    let query: any
    let getUserByIdResponseSuccess: any
    let mockGetUserById: jest.MockedFunction<typeof userRepository.getUserById>

    beforeAll(() => {
      mockGetUserById = userRepository.getUserById as jest.MockedFunction<typeof userRepository.getUserById>
    })

    beforeEach(() => {
      query = {
        id: 1
      }

      getUserByIdResponseSuccess = {
        data: {
          id: 1,
          name: 'example',
          email: 'example@email.com',
          createdAt: '2023-08-03T15:56:17.289Z',
          updatedAt: '2023-08-03T15:56:17.289Z'
        }
      }
    })

    afterEach(() => {
      jest.resetAllMocks()
    })

    test('when input is valid then return successfully', async () => {
      mockGetUserById.mockReturnValueOnce(getUserByIdResponseSuccess.data)
      const response = await userService.getUserById(query.id)
      expect(response).toMatchObject(getUserByIdResponseSuccess.data)
    })
  })

  describe('createUser', () => {
    let createUserRequest: CreateUserRequest
    let createUserResponseSuccess: any
    let mockCreateUser: jest.MockedFunction<typeof userRepository.createUser>

    beforeAll(() => {
      mockCreateUser = userRepository.createUser as jest.MockedFunction<typeof userRepository.createUser>
    })

    beforeEach(() => {
      createUserRequest = {
        id: 1,
        name: 'example',
        email: 'example@email.com'
      }

      createUserResponseSuccess = {
        data: {
          id: 1,
          name: 'example',
          email: 'example@email.com',
          createdAt: '2023-08-03T15:56:17.289Z',
          updatedAt: '2023-08-03T15:56:17.289Z'
        }
      }
    })

    test('when input is valid then return successfully', async () => {
      mockCreateUser.mockReturnValueOnce(createUserResponseSuccess.data)
      jest.spyOn(userServiceHelper, 'mapUsersId').mockReturnValueOnce(createUserResponseSuccess.data)
      jest.spyOn(userServiceHelper, 'findEmptyId').mockReturnValueOnce(createUserResponseSuccess.data)
      const response = await userService.createUser(createUserRequest)
      expect(response).toMatchObject(createUserResponseSuccess.data)
    })
  })

  describe('updateUserById', () => {
    let query: any
    let updateUserByIdRequest: UpdateUserByIdRequest
    let updateUserByIdResponseSuccess: any
    let mockUpdateUserById: jest.MockedFunction<typeof userRepository.updateUserById>

    beforeAll(() => {
      mockUpdateUserById = userRepository.updateUserById as jest.MockedFunction<typeof userRepository.updateUserById>
    })

    beforeEach(() => {
      query = {
        id: 1
      }
      updateUserByIdRequest = {
        name: 'example',
        email: 'example@email.com'
      }
      updateUserByIdResponseSuccess = {
        data: {
          id: 1,
          name: 'example',
          email: 'example@email.com',
          createdAt: '2023-08-03T15:56:17.289Z',
          updatedAt: '2023-08-03T15:56:17.289Z'
        }
      }
    })

    test('when input is valid then return successfully', async () => {
      mockUpdateUserById.mockReturnValueOnce(updateUserByIdResponseSuccess.data)
      const response = await userService.updateUserById(updateUserByIdRequest, query.id)
      expect(response).toMatchObject(updateUserByIdResponseSuccess.data)
    })
  })

  describe('deleteAllUser', () => {
    let mockdeleteAllUser: jest.MockedFunction<typeof userRepository.deleteAllUser>

    beforeAll(() => {
      mockdeleteAllUser = userRepository.deleteAllUser as jest.MockedFunction<typeof userRepository.deleteAllUser>
    })
    test('when input is valid then return successfully', async () => {
      await userService.deleteAllUser()
      expect(mockdeleteAllUser).toHaveBeenCalled()
      expect(mockdeleteAllUser).toHaveBeenCalledTimes(1)
    })
  })

  describe('deleteUserById', () => {
    let query: any
    let mockdeleteUserById: jest.MockedFunction<typeof userRepository.deleteUserById>

    beforeAll(() => {
      mockdeleteUserById = userRepository.deleteUserById as jest.MockedFunction<typeof userRepository.deleteUserById>
    })

    beforeEach(() => {
      query = {
        id: 1
      }
    })

    test('when input is valid then return successfully', async () => {
      await userService.deleteUserById(query.id)
      expect(mockdeleteUserById).toHaveBeenCalled()
      expect(mockdeleteUserById).toHaveBeenCalledTimes(1)
    })
  })
})
