import express, { Express } from 'express'
import swaggerUi from 'swagger-ui-express'
import userController from './routes/UserController'

require('dotenv').config()

const app: Express = express()
const port: string | undefined = process.env.PORT

app.use(express.json())

const expressSwagger = require('express-swagger-generator')(app)
const options = {
  swaggerDefinition: {
    info: {
      description: 'This service is backend service',
      title: 'Swagger',
      version: '1.0.0'
    },
    host: process.env.DOMAINNAME,
    produces: [
      'application/json'
    ],
    schemes: ['https']
  },
  basedir: __dirname,
  files: ['./routes/*.ts', './routes/*.js', './swagger/models/models.js', './swagger/models/models.ts']
}

app.use('/swagger-ui', swaggerUi.serve, swaggerUi.setup(expressSwagger(options)))

app.use('/user', userController)

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`)
})
