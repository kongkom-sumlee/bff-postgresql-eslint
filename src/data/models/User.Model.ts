import { Sequelize, DataTypes } from 'sequelize'
import databaseConfig from '../../config/Database.Config'
import User from '../interfaces/UserAttributes.Interface'

const sequelize = new Sequelize(databaseConfig)

User.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  },
  {
    sequelize,
    modelName: 'User'
  }
);

(async () => {
  try {
    await sequelize.authenticate()
    console.log('Connected to the database')
    await User.sync()
  } catch (error) {
    console.error('Error connecting to the database:', error)
  }
})()

export default User
