import { Model } from 'sequelize'

interface UserAttributes {
    id?: number
    name: string
    email: string
}

class User extends Model<UserAttributes> implements UserAttributes {
    public id!: number
    public name!: string
    public email!: string
    public delete!: boolean

    // Timestamps
    public readonly createdAt!: Date
    public readonly updatedAt!: Date
}

export default User
