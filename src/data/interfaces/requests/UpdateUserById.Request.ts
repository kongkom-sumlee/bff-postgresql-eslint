export interface UpdateUserByIdRequest {
    name: string
    email: string
}
