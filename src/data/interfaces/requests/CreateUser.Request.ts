export interface CreateUserRequest {
    id: number
    name: string
    email: string
}
