import CustomError from '../../utilities/CustomError'
import { CreateUserRequest } from '../interfaces/requests/CreateUser.Request'
import { UpdateUserByIdRequest } from '../interfaces/requests/UpdateUserById.Request'
import User from './../models/User.Model'
import ErrorHandler from '../../utilities/ErrorHandler'

class UserRepository {
  async getAllUser (): Promise<User[]> {
    try {
      const data: User[] = await User.findAll({
        order: [['id', 'ASC']]
      })

      return data
    } catch (error: unknown) {
      throw ErrorHandler.handle(error)
    }
  }

  async getUserById (id: number): Promise<User> {
    try {
      const data = await User.findByPk(id)

      if (!data) {
        throw CustomError.notFound('user not found')
      }

      return data
    } catch (error: unknown) {
      throw ErrorHandler.handle(error)
    }
  }

  async createUser (query: CreateUserRequest): Promise<User> {
    try {
      const data = await User.create({
        id: query.id,
        name: query.name,
        email: query.email
      })

      return data
    } catch (error: unknown) {
      throw ErrorHandler.handle(error)
    }
  }

  async updateUserById (query: UpdateUserByIdRequest, id: number): Promise<User> {
    try {
      const data = await this.getUserById(id)

      if (!data) {
        throw CustomError.notFound('user not found')
      }

      data.name = query.name
      data.email = query.email

      await data.save()

      return data
    } catch (error: any) {
      throw ErrorHandler.handle(error)
    }
  }

  async deleteAllUser () {
    try {
      await User.destroy({
        where: {},
        truncate: true
      })
    } catch (error: any) {
      throw ErrorHandler.handle(error)
    }
  }

  async deleteUserById (id: number) {
    try {
      const user: User = await this.getUserById(id)

      if (!user) {
        throw CustomError.notFound('user not found')
      }

      await User.destroy({
        where: {
          id: id
        },
        restartIdentity: true
      })
    } catch (error: any) {
      throw ErrorHandler.handle(error)
    }
  }
}

export default new UserRepository()
