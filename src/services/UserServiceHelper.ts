import User from '../data/models/User.Model'

class UserServiceHelper {
  public findEmptyId (id: number[]): number {
    let nextId: number = 1

    while (id.includes(nextId)) {
      nextId++
    }

    return nextId
  }

  public mapUsersId (users: User[]) {
    const usersId: number[] = users.map((user) => user.id)

    return usersId
  }
}

export default new UserServiceHelper()
