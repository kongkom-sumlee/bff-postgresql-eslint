import User from '../data/models/User.Model'
import userServiceHelper from './UserServiceHelper'
import userRepository from './../data/repositories/User.Repository'
import { CreateUserRequest } from '../data/interfaces/requests/CreateUser.Request'
import { UpdateUserByIdRequest } from '../data/interfaces/requests/UpdateUserById.Request'
import validator from '../utilities/Validator'
import * as schemaType from '../enums/SchemaType'
import { ValidationResult } from 'joi'

class UserService {
  public async getAllUser (): Promise<User[]> {
    let response: User[]

    response = await userRepository.getAllUser()

    return response
  }

  public async getUserById (paramsId: string): Promise<User> {
    const id: number = parseInt(paramsId)
    let response: User

    response = await userRepository.getUserById(id)

    return response
  }

  public async createUser (query: CreateUserRequest): Promise<User> {
    const validateRequestResult: ValidationResult<any> = validator.validate(query, schemaType.type.CREATE_USER_REQUEST_SCHEMA)
    validator.handleValidateResult(validateRequestResult)

    let response: User
    const users: User[] = await userRepository.getAllUser()
    const userId: number[] = userServiceHelper.mapUsersId(users)
    const emptyUserId: number = userServiceHelper.findEmptyId(userId)

    query.id = emptyUserId
    response = await userRepository.createUser(query)

    return response
  }

  public async updateUserById (query: UpdateUserByIdRequest, paramsId: string): Promise<User> {
    const validateRequestResult = validator.validate(query, schemaType.type.UPDATE_USER_REQUEST_SCHEMA)
    validator.handleValidateResult(validateRequestResult)

    const id: number = parseInt(paramsId)
    let response: User

    response = await userRepository.updateUserById(query, id)

    return response
  }

  public async deleteAllUser () {
    await userRepository.deleteAllUser()
  }

  public async deleteUserById (paramsId: string) {
    const id: number = parseInt(paramsId)

    await userRepository.deleteUserById(id)
  }
}

export default new UserService()
