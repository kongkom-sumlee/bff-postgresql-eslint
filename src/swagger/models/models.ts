/**
 * @typedef Status
 * @property {integer} code
 * @property {string} message
 */

/**
 * @typedef GetAllUserResponseModel
 * @property {Status.model} status
 * @property {Array.<GetAllUserResponseData>} data
 */

/**
 * @typedef GetAllUserResponseData
 * @property {number} id
 * @property {string} name
 * @property {string} email
 * @property {string} createdAt
 * @property {string} updatedAt
 */

/**
 * @typedef GetUserByIdResponseModel
 * @property {Status.model} status
 * @property {GetUserByIdResponseData.model} data
 */

/**
 * @typedef GetUserByIdResponseData
 * @property {number} id
 * @property {string} name
 * @property {string} email
 * @property {string} createdAt
 * @property {string} updatedAt
 */

/**
 * @typedef AddUserByIdRequestModel
 * @property {string} name
 * @property {string} email
 */

/**
 * @typedef AddUserByIdResponseModel
 * @property {Status.model} status
 * @property {AddUserByIdResponseData.model} data
 */

/**
 * @typedef AddUserByIdResponseData
 * @property {number} id
 * @property {string} name
 * @property {string} email
 * @property {string} createdAt
 * @property {string} updatedAt
 */

/**
 * @typedef UpdateUserByIdRequestModel
 * @property {string} name
 * @property {string} email
 */

/**
 * @typedef UpdateUserByIdResponseModel
 * @property {Status.model} status
 * @property {AddUserByIdResponseData.model} data
 */

/**
 * @typedef UpdateUserByIdResponseData
 * @property {number} id
 * @property {string} name
 * @property {string} email
 * @property {string} createdAt
 * @property {string} updatedAt
 */

/**
 * @typedef DeleteAllUserResponseModel
 * @property {Status.model} status
 */

/**
 * @typedef DeleteUserByIdResponseModel
 * @property {Status.model} status
 */
