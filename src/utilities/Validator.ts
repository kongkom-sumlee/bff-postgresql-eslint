import * as schema from './Schema'
import * as schemaType from '../enums/SchemaType'
import CustomError from './CustomError'
import { ObjectSchema, ValidationResult } from 'joi'

type SchemaListType = {
  0: ObjectSchema<any>,
  1: ObjectSchema<any>
}

const SCHEMA_LIST: SchemaListType = {
  [schemaType.type.CREATE_USER_REQUEST_SCHEMA]: schema.CREATE_USER_REQUEST_SCHEMA,
  [schemaType.type.UPDATE_USER_REQUEST_SCHEMA]: schema.UPDATE_USER_REQUEST_SCHEMA
}

const options = {
  errors: {
    wrap: {
      label: ''
    }
  },
  abortEarly: false
}

function validate (value: object, type: schemaType.type) {
  const result = SCHEMA_LIST[type].validate(value, options)

  return result
}

function handleValidateResult (validateResult: ValidationResult<any>) {
  if (validateResult.error) {
    const errorDetail = validateResult.error.details[0]
    const errorMessage = errorDetail.message

    throw CustomError.badRequest(errorMessage, errorDetail)
  }
}

export default {
  validate,
  handleValidateResult
}
