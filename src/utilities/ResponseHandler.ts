import * as errorConstant from './../constants/ErrorConstant'

function generateSuccessResponse (statusCode = 200, data?: any) {
  return {
    status: {
      code: statusCode,
      message: 'success'
    },
    data: data || undefined
  }
}

function generateErrorResponse (statusCode = 500, error = errorConstant.INTERNAL_SERVER_ERROR, errorDescription = 'internal server error', additionalErrorDescription?: any, customCode?: any) {
  return {
    error: {
      code: customCode || statusCode,
      type: error.toUpperCase(),
      description: additionalErrorDescription || errorDescription,
      detail: errorDescription
    }
  }
}

export {
  generateSuccessResponse,
  generateErrorResponse
}
