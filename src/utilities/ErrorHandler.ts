import CustomError from './CustomError'

class ErrorHandler {
  static handle (error: unknown) {
    switch (true) {
      case error instanceof CustomError:
        throw error
      case (error as CustomError).name === 'SequelizeUniqueConstraintError':
        throw CustomError.badRequest('user already exists', (error as CustomError).stack)
      default:
        throw CustomError.dependencyError('got error from database')
    }
  }
}

export default ErrorHandler
