import JoiBase from 'joi'
import JoiDate from '@joi/date'

const Joi = JoiBase.extend(JoiDate) as JoiBase.Root
const IS_REQUIRED = 'is required'
const MUST_NOT_BE_EMPTY = 'must not be empty'
const MUST_BE_A_STRING = 'must be a string'

export const CREATE_USER_REQUEST_SCHEMA = Joi.object({
  name: Joi.string().required()
    .messages({
      'any.required': `{#key} ${IS_REQUIRED}`,
      'string.empty': `{#label} ${MUST_NOT_BE_EMPTY}`,
      'string.base': `{#label} ${MUST_BE_A_STRING}`
    }),
  email: Joi.string()
    .email()
    .required()
    .messages({
      'any.required': `{#key} ${IS_REQUIRED}`,
      'string.empty': `{#key} ${MUST_NOT_BE_EMPTY}`,
      'string.base': `{#key} ${MUST_BE_A_STRING}`
    })
}).unknown(true)

export const UPDATE_USER_REQUEST_SCHEMA = Joi.object({
  name: Joi.string().required()
    .messages({
      'any.required': `{#key} ${IS_REQUIRED}`,
      'string.empty': `{#label} ${MUST_NOT_BE_EMPTY}`,
      'string.base': `{#label} ${MUST_BE_A_STRING}`
    }),
  email: Joi.string()
    .email()
    .required()
    .messages({
      'any.required': `{#key} ${IS_REQUIRED}`,
      'string.empty': `{#key} ${MUST_NOT_BE_EMPTY}`,
      'string.base': `{#key} ${MUST_BE_A_STRING}`
    })
}).unknown(true)
