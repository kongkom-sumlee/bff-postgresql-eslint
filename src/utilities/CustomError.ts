import * as errorConstant from '../constants/ErrorConstant'

export default class CustomError extends Error {
    statusCode: number
    error: any
    errorDescription?: any
    stack: any
    additionalErrorDescription: any
    customCode: any
    __proto__ = Error

    constructor (statusCode: number, error: any, errorDescription?: any, stack?: any, additionalErrorDescription?: any, customCode?: any) {
      super()
      this.statusCode = statusCode
      this.error = error
      this.errorDescription = errorDescription
      this.additionalErrorDescription = additionalErrorDescription
      this.customCode = customCode

      Object.defineProperty(this, 'stack', {
        value: stack,
        writable: true,
        enumerable: true
      })
      Object.setPrototypeOf(this, CustomError.prototype)
    }

    static badRequest (errorDescription?: any, stack?: any, additionalErrorDescription?: any) {
      return new CustomError(400, errorConstant.INVALID_PARAMS, errorDescription, stack, additionalErrorDescription)
    }

    static notFound (errorDescription?: any, stack?: any, additionalErrorDescription?: any) {
      return new CustomError(404, errorConstant.DATA_NOT_FOUND, errorDescription, stack, additionalErrorDescription)
    }

    static internalServerError (errorDescription?: any, stack?: any, additionalErrorDescription?: any) {
      return new CustomError(500, errorConstant.INTERNAL_SERVER_ERROR, errorDescription, stack, additionalErrorDescription)
    }

    static dependencyError (errorDescription?: any, stack?: any, additionalErrorDescription?: any) {
      return new CustomError(500, errorConstant.DEPENDENCY_ERROR, errorDescription, stack, additionalErrorDescription)
    }
}
