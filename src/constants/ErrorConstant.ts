export const INTERNAL_SERVER_ERROR: string = 'internal_server_error'
export const DATA_NOT_FOUND: string = 'data_not_found'
export const INVALID_PARAMS: string = 'invalid_params'
export const DEPENDENCY_ERROR: string = 'dependency_error'
