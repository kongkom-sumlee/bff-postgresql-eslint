import express, { IRouter, Request, Response } from 'express'
import User from './../data/models/User.Model'
import userService from './../services/UserService'
import { generateErrorResponse, generateSuccessResponse } from '../utilities/ResponseHandler'
import CustomError from '../utilities/CustomError'

const router: IRouter = express.Router()

/**
 * This function is used to get all user
 * @route GET /user/v1/users
 * @summary Get all user
 * @group User - User Service
 * @returns {GetAllUserResponseModel.model} 200 - return response get all user
 */
router.get('/v1/users', async (req: Request, res: Response) => {
  try {
    const response = await userService.getAllUser()

    res.status(200).json(generateSuccessResponse(200, response))
  } catch (error) {
    let errorResponse: object = {}

    if (error instanceof CustomError) {
      errorResponse = generateErrorResponse(error.statusCode, error.error, error.errorDescription, error.additionalErrorDescription, error.customCode)
      res.status(error.statusCode).json(errorResponse)
    } else {
      errorResponse = generateErrorResponse()
      res.status(500).json(errorResponse)
    }
  }
})

/**
 * This function is used to get user by id
 * @route GET /user/v1/user/{id}
 * @summary Get user by id
 * @group User - User Service
 * @param {string} id.path.required - user id
 * @returns {GetUserByIdResponseModel.model} 200 - return response get user by id
 */
router.get('/v1/user/:id', async (req: Request, res: Response) => {
  try {
    const response = await userService.getUserById(req.params.id)

    res.status(200).json(generateSuccessResponse(200, response))
  } catch (error) {
    let errorResponse: object = {}

    if (error instanceof CustomError) {
      errorResponse = generateErrorResponse(error.statusCode, error.error, error.errorDescription, error.additionalErrorDescription, error.customCode)
      res.status(error.statusCode).json(errorResponse)
    } else {
      errorResponse = generateErrorResponse()
      res.status(500).json(errorResponse)
    }
  }
})

/**
 * This function is used to add user
 * @route Post /user/v1/user
 * @summary Add user
 * @group User - User Service
 * @param {AddUserByIdRequestModel.model}  Body.body.required - Body request
 * @returns {AddUserByIdResponseModel.model} 200 - return response add user
 */
router.post('/v1/user', async (req: Request, res: Response) => {
  try {
    const response = await userService.createUser(req.body)

    res.status(200).json(generateSuccessResponse(200, response))
  } catch (error) {
    let errorResponse: object = {}

    if (error instanceof CustomError) {
      errorResponse = generateErrorResponse(error.statusCode, error.error, error.errorDescription, error.additionalErrorDescription, error.customCode)
      res.status(error.statusCode).json(errorResponse)
    } else {
      errorResponse = generateErrorResponse()
      res.status(500).json(errorResponse)
    }
  }
})

/**
 * This function is used to add user by id
 * @route Put /user/v1/user/{id}
 * @summary Update user by id
 * @group User - User Service
 * @param {string} id.path.required - user id
 * @param {UpdateUserByIdRequestModel.model}  Body.body.required - Body request
 * @returns {UpdateUserByIdResponseModel.model} 200 - return response update user by id
 */
router.put('/v1/user/:id', async (req: Request, res: Response) => {
  try {
    const response = await userService.updateUserById(req.body, req.params.id)

    res.status(200).json(generateSuccessResponse(200, response))
  } catch (error) {
    let errorResponse: object = {}

    if (error instanceof CustomError) {
      errorResponse = generateErrorResponse(error.statusCode, error.error, error.errorDescription, error.additionalErrorDescription, error.customCode)
      res.status(error.statusCode).json(errorResponse)
    } else {
      errorResponse = generateErrorResponse()
      res.status(500).json(errorResponse)
    }
  }
})

/**
 * This function is used to delete all user
 * @route Delete /user/v1/users
 * @summary Delete all user
 * @group User - User Service
 * @returns {DeleteAllUserResponseModel.model} 200 - return response delete all user
 */
router.delete('/v1/users', async (req: Request, res: Response) => {
  try {
    await User.destroy({
      where: {},
      truncate: true
    })

    res.status(200).json(generateSuccessResponse(200))
  } catch (error) {
    let errorResponse: object = {}

    if (error instanceof CustomError) {
      errorResponse = generateErrorResponse(error.statusCode, error.error, error.errorDescription, error.additionalErrorDescription, error.customCode)
      res.status(error.statusCode).json(errorResponse)
    } else {
      errorResponse = generateErrorResponse()
      res.status(500).json(errorResponse)
    }
  }
})

/**
 * This function is used to delete user by id
 * @route Delete /user/v1/user/{id}
 * @summary Delete user by id
 * @group User - User Service
 * @param {string} id.path.required - user id
 * @returns {DeleteUserByIdResponseModel.model} 200 - return response delete user by id
 */
router.delete('/v1/user/:id', async (req: Request, res: Response) => {
  try {
    await userService.deleteUserById(req.params.id)

    res.status(200).json(generateSuccessResponse(200))
  } catch (error) {
    let errorResponse: object = {}

    if (error instanceof CustomError) {
      errorResponse = generateErrorResponse(error.statusCode, error.error, error.errorDescription, error.additionalErrorDescription, error.customCode)
      res.status(error.statusCode).json(errorResponse)
    } else {
      errorResponse = generateErrorResponse()
      res.status(500).json(errorResponse)
    }
  }
})

export default router
