import { SequelizeOptions } from 'sequelize-typescript'
import { Dialect } from 'sequelize'

require('dotenv').config()

const dialect: Dialect = 'postgres'
const host: string | undefined = process.env.POSTGRESQL_HOST
const port: number | undefined = Number(process.env.POSTGRESQL_PORT)
const username: string | undefined = process.env.POSTGRESQL_USERNAME
const password: string | undefined = process.env.POSTGRESQL_PASSWORD
const database: string | undefined = process.env.POSTGRESQL_DATABASE

const config: SequelizeOptions = {
  dialect: dialect,
  host: host,
  port: port,
  username: username,
  password: password,
  database: database,
  dialectOptions: {
    ssl: {
      require: true
    }
  }
}

export default config
